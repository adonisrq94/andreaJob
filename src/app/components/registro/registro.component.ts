import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],

})
export class RegistroComponent implements OnInit {

  public formulario:FormGroup;

  public password_repeat: boolean = true;

    constructor() {

      this.formulario = new FormGroup(
      {

         'nombre': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z]+)"),Validators.minLength(3)]),
         'segundonombre': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z]+)"),Validators.minLength(3)]),
         'apellido': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z]+)"),Validators.minLength(3)]),
         'segundoapellido': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z]+)"),Validators.minLength(3)]),
         'telefono': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z0-9]+)"),Validators.minLength(7)]),
         'direccion': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z0-9]+)"),Validators.minLength(3)]),
         'email': new FormControl('',[Validators.required]),
         'username': new FormControl('',[Validators.required,Validators.pattern("([A-Za-z0-9]+)"),Validators.minLength(3)]),
         'password': new FormControl('',[Validators.required,Validators.minLength(8)]),
         'genero': new FormControl('',[Validators.required]),
         'confirmarpassword': new FormControl('',[Validators.required,Validators.minLength(8)]),
         'edad': new FormControl(),
      })
    }
// Funcion para Comparar ambas Contraseñas
public comparar(): void{

  if (this.formulario.value.password == this.formulario.value.confirmarpassword){
    this.password_repeat = true;
  }else
    this.password_repeat=false;
}

// Hashear la contraseña
// public hash(password:string): void{
//   HASH(REPLACE(password; " "; "_")),
//   HASH(password; 10),



    ngOnInit() {
    }

    nuevoPrograma(){
      this.comparar();
      console.log(this.password_repeat)
      if (this.password_repeat){
      console.log("value",this.formulario)
      }
}
      resetear(){
        console.log("hola");


}
}
